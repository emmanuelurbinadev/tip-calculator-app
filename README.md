# Frontend Mentor - Tip calculator app solution

This is a solution to the [Tip calculator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/tip-calculator-app-ugJNGbJUX). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - Tip calculator app solution](#frontend-mentor---tip-calculator-app-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
  - [Desktop](#desktop)
  - [Mobile](#mobile)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
  - [Author](#author)


## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Calculate the correct tip and total cost of the bill per person

### Screenshot

## Desktop

![app desktop](./design/app.png)

## Mobile

![app desktop](./design/appm.png)

### Links

- Solution URL: [Gitlab Code Solution](https://gitlab.com/emmanuelurbinadev/tip-calculator-app)
- Live Site URL: [Add live site URL](https://symphonious-druid-e03cd5.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid


### What I learned

I learned how styled checkbox and manage styles with css

To see how you can add code snippets, see below:

```html
<div class="control__tip">
  <input type="radio" id="cbx5" value="50" name="tipe" onclick="setValue(this.value)">
  <label for="cbx5">50%</label>
</div>
```
```css
.control__tip {
    width: 100%;
    height: auto;

}

.control__tip label {
    display: inline-block;
    font-size: 18px;
    width: 100%;
    height: auto;
    padding: 1em;
    border-radius: 8px;
    background-color: var(--very-dark-cyan);
    color: var(--white);
}

.control__tip input[type=radio] {
    display: none;
}

.control__tip input:checked+label {
    background-color: var(--strong-cyan);
    color: var(--very-dark-cyan);
}
```

## Author

- Website - [Blog](https://vidaencodigo.wordpress.com/)
- Frontend Mentor - [@emmanuelurbina](https://www.frontendmentor.io/profile/emmanuelurbina)
- Twitter - [@emmanuelluur](https://www.twitter.com/emmanuelluur)
