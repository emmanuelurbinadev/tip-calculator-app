class Tip {


    convert_percent() {
        return this.percent / 100
    }

    calculate_tip() {
        let tip_percent = (this.convert_percent() * this.total)
        return tip_percent
    }

    tip_amount() {
        let total = 0

        let amount = this.calculate_tip() / this.people
        total = amount.toFixed(2)
        return total
    }
    tip_total() {
        let total = parseFloat(this.total)
        let tip = parseFloat(this.calculate_tip())
        let people = this.people
        return ((total + tip) / people).toFixed(2)
    }
}
const Tip_1 = new Tip()
Tip_1.total = 0
Tip_1.percent = 0
Tip_1.people = 0
document.addEventListener("DOMContentLoaded", () => {
    let reset_btn = document.querySelector("#reset")


    reset_btn.addEventListener("click", () => {
        document.querySelector("#custom-tip").value = null
        document.querySelector("#bill").value = null
        document.querySelector("#n_people").value = null
        document.querySelector("#tip__amount").innerHTML = " 0.00"
        document.querySelector("#tip__total").innerHTML = " 0.00"
        uncheck()
        document.querySelector("#bill").focus()
    })


})

function uncheck() {
    try {
        let element = document.querySelector('input[type=radio][name=tipe]:checked');
        element.checked = false;
    } catch (e) {
        return null
    }
}

function setTotal(e) {
    Tip_1.total = e
}


function setValue(e) {
    Tip_1.percent = e
    if (Tip_1.total != 0 || Tip_1.people != 0) calc(Tip_1.people)
}

function setPeople(e) {
    Tip_1.people = e

}


function calc(e) {
    if (e == 0 || e == 'null' || Tip_1.percent === 0) {
        document.querySelector(".error__mess").style.display = 'block'
        document.querySelector('#n_people').classList.add("error")

    } else {
        document.querySelector(".error__mess").style.display = 'none'
        document.querySelector('#n_people').classList.remove("error")
        setTip()
        tip__total()
    }
}

function setTip() {
    document.querySelector("#tip__amount").innerHTML = Tip_1.tip_amount()
}

function tip__total() {
    document.querySelector("#tip__total").innerHTML = Tip_1.tip_total()
}